﻿open Microsoft.Z3
open System

let sudoku = 
       use ctx = new Context()
       use solver = ctx.MkSolver()
       let mkInt (n:int) = ctx.MkInt(n)
       let mkVar (s:string) = ctx.MkConst (s, ctx.MkIntSort()):?>ArithExpr
       let mkeq v k = ctx.MkEq(v, mkInt k)
       
       let xss = [
                [None; None; None; None; None; Some 9; Some 1; None; Some 3];
                [None; None; None; None; Some 8; None; Some 9; None; None];
                [None; None; Some 5; Some 4; None; None; None; None; None];
                [Some 7; None; Some 6; None; Some 4; None;  Some 8; None; None];
                [Some 4; None; None; None; Some 9; None; Some 7; None; None];
                [None; None; Some 3; Some 2; Some 7;  None; Some 4; Some 1; None];
                [None; None; None; None; Some 1; None; Some 5; None; None];
                [None; None; None; Some 7; Some 5; None; None; Some 2; None];
                [Some 3; None; None; Some 8; None; None; None; None; None]
             ]
       let board = [for i in [0..8] do yield [for j in [0..8] do yield mkVar ("s" + i.ToString() + j.ToString())]]

       let rec smth = function 
            | None -> []
            | Some x -> x
   
       let OneToNine xss = 
                List.iter (
                  fun xs -> // xs - row of variables
                    List.iter (
                     fun k ->
                       // solver.Assert (List.fold (fun x y -> ctx.MkOr(x, mkeq y k)) (ctx.MkFalse()) xs) 
                        solver.Assert (List.fold (fun x y -> ctx.MkOr(x, ctx.MkEq(y, mkInt(k)))) (ctx.MkFalse()) xs)  //for random   
                     ) [1..9] 
                ) xss
       let rec cols = function
                    | [xs] -> [for x in xs do yield [x]]
                    | (ys :: zss) -> List.map (fun (y, zs) -> y :: zs) (List.zip ys (cols zss))
       
 //      for i in [0..8] do
   //     solver.Assert(mkeq (board.Item(i).Item(i))(i+1))
       
       OneToNine board
       OneToNine (cols board)
       solver.Check().ToString() 

       let rec take n = function
                |[]->[]
                |(x::xs) -> if n = 0 then []
                            else x::(take(n-1)xs) //[1 3 5]-> [1 3]

       let rec drop n = function
                |[] -> []
                |(x::xs) -> if n = 0 then (x::xs)
                            else drop (n-1) xs //[1 3 5]-> [3 5]

       let rec group = function
                |[]->[]
                |xs -> (take 3 xs) :: (group (drop 3 xs)) //[ [0 1 2 ] [3 4 5] [6 7 8]] 

       let boxes xss = List.map List.concat (List.concat(List.map cols (group (List.map group xss))))

       OneToNine (boxes board)
       
       let res =solver.Check()
       let displayboard = 
                for row in board do 
                    Console.WriteLine()
                    for v in row do
                        Console.Write(solver.Model.ConstInterp(v).ToString() + " ")
       if res.ToString() = "SATISFIABLE" then displayboard
       res.ToString()


                    

[<EntryPoint>]

let main argv =
 Console.WriteLine(sudoku)
 0